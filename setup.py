from setuptools import setup, find_packages
import sys, os

version = '1.0.3'

setup(name='zipcodepy',
      version=version,
      description="query zip codes and location data",
      long_description=open("README.md").read(),
      long_description_content_type="text/markdown",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='zip code distance',
      author='Jinx',
      author_email='jinx@oyoclass.com',
      url='https://gitlab.com/oyoclass/zipcodepy3',
      license='GPL',
      packages=find_packages(exclude=['ez_setup', 'examples', 'tests']),
      zip_safe=False,
      include_package_data=True,
      install_requires=[],
      entry_points="""
      # -*- Entry points: -*-
      """,
)
